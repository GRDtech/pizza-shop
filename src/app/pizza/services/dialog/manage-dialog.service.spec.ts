import { TestBed } from '@angular/core/testing';

import { ManageDialogService } from './manage-dialog.service';

describe('ManageDialogService', () => {
  let service: ManageDialogService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ManageDialogService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
