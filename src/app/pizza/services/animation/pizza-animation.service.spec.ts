import { TestBed } from '@angular/core/testing';

import { PizzaAnimationService } from './pizza-animation.service';

describe('PizzaAnimationService', () => {
  let service: PizzaAnimationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PizzaAnimationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
