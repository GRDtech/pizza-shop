import { Injectable } from "@angular/core";
import { PizzaToppingsClass } from '../pizza-toppings.class';
import { PizzaToppingsInterface } from '../pizza-toppings.interface';
import { IPizzaToppingsAdapter } from './pizza-toppings.adapter.interface';


@Injectable({
    providedIn: 'root'
})
export class PizzaToppignsAdapter implements IPizzaToppingsAdapter<PizzaToppingsClass>{
    constructor(){}
    pizzaAdapt(topping: PizzaToppingsInterface): PizzaToppingsClass {
        return new PizzaToppingsClass(topping.toppingID, topping.toppingName, topping.isSelected);
    }
}