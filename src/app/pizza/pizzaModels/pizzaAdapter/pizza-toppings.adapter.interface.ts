import { PizzaToppingsInterface } from '../pizza-toppings.interface';

export interface IPizzaToppingsAdapter <T> {
    pizzaAdapt(topping: PizzaToppingsInterface): T;
}
