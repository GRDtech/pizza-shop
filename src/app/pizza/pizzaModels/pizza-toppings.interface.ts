export interface PizzaToppingsInterface{
    toppingID: number;
    toppingName: string;
    isSelected?: boolean;
  };