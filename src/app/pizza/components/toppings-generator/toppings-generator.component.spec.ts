import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ToppingsGeneratorComponent } from './toppings-generator.component';

describe('ToppingsGeneratorComponent', () => {
  let component: ToppingsGeneratorComponent;
  let fixture: ComponentFixture<ToppingsGeneratorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ToppingsGeneratorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToppingsGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
