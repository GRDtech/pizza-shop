import { ChangeDetectionStrategy, Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { defaultEnterLeaveAnimation } from '../../services/animation/default-animation';
// import { ManageDialogService } from '../../services/dialog/manage-dialog.service';

@Component({
  selector: 'pizza-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css'],
  animations: [
    defaultEnterLeaveAnimation()
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DialogComponent implements OnInit {
  
  dialog$: Observable<{isDialogOpen: boolean, isDialogData: unknown}>;
  isMediumScreen: boolean;
  contactDetailsForm: FormGroup;
  returnDialogData ;
  @Input('isMedium') set screenSize(size: boolean){
    this.isMediumScreen = size;
    console.log('from dialod size ',this.isMediumScreen)
  }
  @Output() hideBottomBar: EventEmitter<boolean> = new EventEmitter<boolean>(false);
  @Output() confirmOrder: EventEmitter<unknown> = new EventEmitter<unknown>();

  constructor(
              private fb: FormBuilder,
              public dialogRef: MatDialogRef<DialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.contactDetailsForm = this.fb.group({
      'firstName': ['', Validators.required],
      'lastName': ['', Validators.required],
      'address': ['', Validators.required]
    });
    this.returnDialogData = { dialogData: this.contactDetailsForm.value, closeValue: true}
    // this.dialog$ = this.dialogService.getDialogObservable();
  }
  // close(){
  //  // this.dialogService.onDialogOpen(false);
  //   this.hideBottomBar.emit(false);
  // }
  // confrim(){
  //   console.log('confirm');
  //   this.confirmOrder.emit();
  //  // this.dialogService.onDialogOpen(false);
  //   this.hideBottomBar.emit(false);
  //   this.confirmOrder.emit();
  // }
}
