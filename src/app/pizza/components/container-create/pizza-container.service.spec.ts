import { TestBed } from '@angular/core/testing';

import { PizzaContainerService } from './pizza-container.service';

describe('PizzaContainerService', () => {
  let service: PizzaContainerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PizzaContainerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
