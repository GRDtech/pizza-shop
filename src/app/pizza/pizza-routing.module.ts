import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConfirmOrderComponent } from './components/confirm-order/confirm-order.component';
import { ContainerCreateComponent } from './components/container-create/container-create.component';
import { MainLandingPageComponent } from './main-landing-page/main-landing-page.component';

const routes: Routes = [
  {
    path: '',
    component: MainLandingPageComponent
  },
  {
    path: 'build-pizza',
    component: ContainerCreateComponent,
    children:[
      {
        path:'c',
        component: ConfirmOrderComponent
      }
    ]
  },
  {
    path: 'confirm',
    component: ConfirmOrderComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PizzaRoutingModule { }
